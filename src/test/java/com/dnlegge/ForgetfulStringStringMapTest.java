package com.dnlegge;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Contract tests that rely only on methods available on the interface
 **/
class ForgetfulStringStringMapTest {

    @Test
    void maxSizeShouldNotbeLessThan1() {

        try {
            new ForgetfulMap(0);
            fail("constructor parameter of 0 should have caused exception");
        } catch (IllegalArgumentException e) {
            //swallow
        }

    }

    @Test
    void newlyConstructedForgetfulMapShouldBeEmpty() {

        ForgetfulStringStringMap forgetfulStringMap = new ForgetfulMap(1);

        String anything = forgetfulStringMap.find("anything");

        assertNull(anything, "Newly constructed forgetful map should return null on find");

    }

    @Test
    void forgetfulStringMapShouldAddAndRetrieveKeyValuePairCorrectly() {

        ForgetfulStringStringMap forgetfulMap = new ForgetfulMap(1);

        forgetfulMap.add("key", "value");

        String value = forgetfulMap.find("key");

        assertNotNull( value, "Retrieved value should not be null");
        assertEquals("value", value, "Retrieved value should be same as stored");

    }

    @Test
    void forgetfulMapShouldReplaceTheLeastUsedKvPair() {

        ForgetfulStringStringMap forgetfulMap = new ForgetfulMap(4);

        forgetfulMap.add("key1", "value1");
        forgetfulMap.add("key2", "value2");
        forgetfulMap.add("key3", "value3");
        forgetfulMap.add("key4", "value4");

        forgetfulMap.find("key1");
        forgetfulMap.find("key2");
        forgetfulMap.find("key4");

        forgetfulMap.add("key5", "value5");

        // ensure keys 1, 2, 4 and 5 are still in the map but key3 isn't

        assertEquals("value1",forgetfulMap.find("key1"), "Lookup of key1 returned incorrect value");
        assertEquals("value2",forgetfulMap.find("key2"), "Lookup of key2 returned incorrect value");

        assertNull(forgetfulMap.find("key3"), "Lookup of key3 should have returned null");

        assertEquals("value4",forgetfulMap.find("key4"), "Lookup of key4 returned incorrect value");
        assertEquals("value5",forgetfulMap.find("key5"), "Lookup of key5 returned incorrect value");

        //then add a sixth key to replace number 4 (run find on all the others first)
        forgetfulMap.find("key1");
        forgetfulMap.find("key1");

        forgetfulMap.find("key2");
        forgetfulMap.find("key2");

        forgetfulMap.find("key5");
        forgetfulMap.find("key5");
        forgetfulMap.find("key5");

        forgetfulMap.add("key6", "value6");

        assertEquals("value1",forgetfulMap.find("key1"), "Lookup of key1 returned incorrect value");
        assertEquals("value2",forgetfulMap.find("key2"), "Lookup of key2 returned incorrect value");

        assertNull(forgetfulMap.find("key3"), "Lookup of key3 should have returned null");
        assertNull(forgetfulMap.find("key4"), "Lookup of key4 should have returned null");

        assertEquals("value5",forgetfulMap.find("key5"), "Lookup of key5 returned incorrect value");
        assertEquals("value6",forgetfulMap.find("key6"), "Lookup of key6 returned incorrect value");

    }

    @Test
    void inTheEventOfTieBreakForgetfulMapShouldReplaceTheOlderValue() {

        ForgetfulStringStringMap forgetfulMap = new ForgetfulMap(4);

        // add keys 1 to 4 (to fill map to max capacity)
        forgetfulMap.add("key1", "value1");
        forgetfulMap.add("key2", "value2");
        forgetfulMap.add("key3", "value3");
        forgetfulMap.add("key4", "value4");

        // call find on keys 1 and 3, leaving tie-break between 2 and 4
        forgetfulMap.find("key1");
        forgetfulMap.find("key3");

        // now add key5, meaning key 2 as oldest value is discarded
        forgetfulMap.add("key5", "value5");

        // ensure keys 1, 3, 4 and 5 are still in the map but key3 isn't

        assertEquals("value1",forgetfulMap.find("key1"), "Lookup of key1 returned incorrect value");

        assertNull(forgetfulMap.find("key2"), "Lookup of key2 should have returned null");

        assertEquals("value3",forgetfulMap.find("key3"), "Lookup of key3 returned incorrect value");
        assertEquals("value4",forgetfulMap.find("key4"), "Lookup of key4 returned incorrect value");
        assertEquals("value5",forgetfulMap.find("key5"), "Lookup of key5 returned incorrect value");

        //then add a sixth key to replace number 4 (run find on all the others first)

        forgetfulMap.add("key6", "value6");

        assertEquals("value1", forgetfulMap.find("key1"), "Lookup of key1 returned incorrect value");
        assertNull(forgetfulMap.find("key2"), "Lookup of key2 should have returned null");

        assertEquals("value3", forgetfulMap.find("key3"), "Lookup of key3 should have returned null");
        assertNull(forgetfulMap.find("key4"), "Lookup of key4 should have returned null");

        assertEquals("value5",forgetfulMap.find("key5"), "Lookup of key5 returned incorrect value");
        assertEquals("value6",forgetfulMap.find("key6"), "Lookup of key6 returned incorrect value");

    }
}