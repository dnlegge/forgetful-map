package com.dnlegge;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

/**
 * These are 'white-box' unit tests that rely on methods on the
 * concrete class itself
**/
class ForgetfulMapTest {

    @Test
    void newlyConstructedForgetfulMapShouldReportMaxSizeCorrectly() {

        ForgetfulMap forgetfulStringMap = new ForgetfulMap(1);

        assertEquals(1, forgetfulStringMap.getMaxSize());

    }

    @Test
    void forgetfulMapShouldReportCurrentSizeCorrectly() {

        ForgetfulMap forgetfulMap = new ForgetfulMap(1);

        assertEquals(0, forgetfulMap.getCurrentSize());

        forgetfulMap.add("key", "value");

        assertEquals(1, forgetfulMap.getCurrentSize());

    }

    @Test
    void forgetfulMapShouldNotAllowDuplicates() {

        ForgetfulMap forgetfulMap = new ForgetfulMap(2);

        assertEquals(0, forgetfulMap.getCurrentSize());

        forgetfulMap.add("key", "value");

        assertEquals(1, forgetfulMap.getCurrentSize());

        forgetfulMap.add("key", "value");
        forgetfulMap.add("key", "value");
        forgetfulMap.add("key", "value");
        forgetfulMap.add("key", "value");
        assertEquals(1, forgetfulMap.getCurrentSize());

    }

    @Test
    void forgetfulMapShouldNotLoseEntryIfDuplicateIsBeingAdded() {

        ForgetfulMap forgetfulMap = new ForgetfulMap(3);

        assertEquals(0, forgetfulMap.getCurrentSize());

        forgetfulMap.add("key1", "value1");

        assertEquals(1, forgetfulMap.getCurrentSize());

        forgetfulMap.add("key2", "value2");
        forgetfulMap.add("key3", "value3");

        assertEquals(3, forgetfulMap.getCurrentSize());

        forgetfulMap.add("key3", "value3");
        assertEquals(3, forgetfulMap.getCurrentSize());

    }

    @Test
    void forgetfulMapShouldNotHoldMoreThanMaxCapacityAndNewerValueIsInserted() {

        ForgetfulMap forgetfulMap = new ForgetfulMap(1);

        assertEquals(0, forgetfulMap.getCurrentSize());

        forgetfulMap.add("key", "value");

        assertEquals(1, forgetfulMap.getCurrentSize());

        forgetfulMap.add("key2", "value2");

        assertEquals(1, forgetfulMap.getCurrentSize());

        String actual = forgetfulMap.find("key2");
        assertNotNull(actual, "With max size of 1, a new value should replace the existing");
        assertEquals("value2", actual, "With max size of 1, a new value should replace the existing");

    }

    @Test
    void forgetfulMapShouldReplaceTheLeastUsedKvPair() {

        ForgetfulMap forgetfulMap = new ForgetfulMap(2);

        assertEquals(0, forgetfulMap.getCurrentSize(), "current size should still be 0");

        forgetfulMap.add("key", "value");

        assertEquals(1, forgetfulMap.getCurrentSize(), "current size should still be 1");

        forgetfulMap.add("key2", "value2");

        assertEquals(2, forgetfulMap.getCurrentSize(), "current size should be 2");

        // call find on the second key so it has more usage than first
        String retrieve2ndKey = forgetfulMap.find("key2");
        assertNotNull(retrieve2ndKey, "Lookup of key2 returned null");
        assertEquals("value2", retrieve2ndKey, "Lookup of key2 returned incorrect value");

        forgetfulMap.add("key3", "value3");

        assertEquals(2, forgetfulMap.getCurrentSize(), "current size should still be 2 (maximum)");

        //ensure key2 and key3 are still in the map but key1 isn't
        assertNull(forgetfulMap.find("key1"), "Lookup of key1 should have returned null");

        String key2LookupResult = forgetfulMap.find("key2");
        assertNotNull(key2LookupResult, "Lookup of key2 returned null");
        assertEquals("value2", key2LookupResult, "Lookup of key2 returned incorrect value");

        String key3LookupResult = forgetfulMap.find("key3");
        assertNotNull(key3LookupResult, "Lookup of key3 failed - A new value should have replaced key1");
        assertEquals("value3", key3LookupResult, "Lookup of key3 failed - incorrect value returned");

    }

}