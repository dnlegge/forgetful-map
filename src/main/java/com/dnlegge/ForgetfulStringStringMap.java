package com.dnlegge;

public abstract class ForgetfulStringStringMap extends AbstractForgetfulMap<String, String> {

    protected ForgetfulStringStringMap(int maxSize) {
        super(maxSize);
    }

}
