package com.dnlegge;

import org.apache.commons.lang3.tuple.MutableTriple;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public abstract class AbstractForgetfulMap<K, V> implements GenericForgetfulMap<K, V> {

    private long insertionIndex = 0;
    private final int maxSize;
    private final ConcurrentMap<K, MutableTriple<V, Integer, Long>> forgettingMap;

    private AbstractForgetfulMap() {
        throw new IllegalArgumentException("Cannot create ForgetfulMap without size");
    }

    public AbstractForgetfulMap(int maxSize) {
        if (maxSize < 1) {
            throw new IllegalArgumentException("Max size of ForgetfulMap cannot be less than 1");
        }
        this.maxSize = maxSize;
        this.forgettingMap = new ConcurrentHashMap<>();
    }

    public int getMaxSize() {
        return maxSize;
    }

    public int getCurrentSize() {
        synchronized (this) {
            return forgettingMap.size();
        }
    }

    public void add(K key, V value) {
        synchronized (this) {
            if (!forgettingMap.containsKey(key) && getCurrentSize() >= maxSize) {
                removeLeastUsedOldestValueFromMap();
            }

            forgettingMap.put(key, new MutableTriple<>(value, 0, ++insertionIndex));
        }
    }

    private void removeLeastUsedOldestValueFromMap() {
        Map.Entry<K, MutableTriple<V, Integer, Long>> leastUsedEntry = null;
        for (Map.Entry<K, MutableTriple<V, Integer, Long>> entry : forgettingMap.entrySet()) {
            if (leastUsedEntry == null
                    || leastUsedEntry.getValue().getMiddle() > entry.getValue().getMiddle()
                    ||
                        (leastUsedEntry.getValue().getMiddle().equals(entry.getValue().getMiddle())
                                && leastUsedEntry.getValue().getRight() > entry.getValue().getRight())
            ) {
                leastUsedEntry = entry;
            }
        }

        //if leastUsedEntry is null here, we *want* an exception
        assert leastUsedEntry != null;
        forgettingMap.remove(leastUsedEntry.getKey());
    }

    public V find(K key) {
        synchronized (this) {
            MutableTriple<V, Integer, Long> valueTriple = forgettingMap.get(key);
            if (valueTriple == null) {
                return null;
            }

            valueTriple.setMiddle(valueTriple.getMiddle() + 1);
            forgettingMap.replace(key, valueTriple);
            return valueTriple.getLeft();
        }
    }

}
