package com.dnlegge;

public interface GenericForgetfulMap<K, V> {

    void add(K key, V value);

    V find(K key);

}

